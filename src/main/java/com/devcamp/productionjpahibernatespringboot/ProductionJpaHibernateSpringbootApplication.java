package com.devcamp.productionjpahibernatespringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductionJpaHibernateSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductionJpaHibernateSpringbootApplication.class, args);
	}

}
