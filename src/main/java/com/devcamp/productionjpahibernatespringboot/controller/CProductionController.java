package com.devcamp.productionjpahibernatespringboot.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.productionjpahibernatespringboot.model.CProduction;
import com.devcamp.productionjpahibernatespringboot.repository.IProductionRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CProductionController {

    @Autowired
    IProductionRepository vProductionRepository;

    @GetMapping("/production")
    public ResponseEntity<List<CProduction>> getAllProduction() {
        try {
            ArrayList<CProduction> listProduction = new ArrayList<>();
            vProductionRepository.findAll().forEach(listProduction::add);
            if (listProduction.size() == 0) {
                return new ResponseEntity<>(listProduction, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(listProduction, HttpStatus.OK);
            }

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
