package com.devcamp.productionjpahibernatespringboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.productionjpahibernatespringboot.model.CProduction;

public interface IProductionRepository extends JpaRepository<CProduction, Long> {

}
